import React from 'react';
import Navbar from "react-bootstrap/Navbar";
import {Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import News from "./Components/News";
import Categories from "./Components/Categories";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import Category from "./Components/Category";

function App() {
    return (
        <>
            <Router>
                <Container className="mb-4">
                    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                        <Navbar.Brand><Link to="/">Divergent News App</Link></Navbar.Brand>
                    </Navbar>
                </Container>
                <Container>
                    <Row>
                        <Col lg={9}>
                            <Switch>
                                <Route path="/category/:id" children={<Category/>}/>
                                <Route path="/">
                                    <News/>
                                </Route>
                            </Switch>
                        </Col>
                        <Col lg={3}>
                            <Categories/>
                        </Col>
                    </Row>
                </Container>
            </Router>
        </>
    );
}

export default App;
