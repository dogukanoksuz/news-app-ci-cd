import React, {Component} from 'react';
import Card from "react-bootstrap/Card";

class News extends Component {
    state = {
        isLoading: true,
        news: [],
        error: null
    };

    fetchNews() {
        fetch(`http://apisrv.local/`)
            .then(response => response.json())
            .then(data => this.setState({
                news: data,
                isLoading: false
            }))
            .catch(error => this.setState({error, isLoading: false}))
    }

    componentDidMount() {
        this.fetchNews();
    }

    render() {
        const {isLoading, news, error} = this.state;

        return (
            <>
                {error ? <p>{error.message}</p> : null}
                {!isLoading ? (
                    news.map(article => {
                        const {id, title, content, created_at} = article;
                        return (
                            <Card key={id} className="mb-4">
                                <Card.Header as="h5">{title}</Card.Header>
                                <Card.Body>
                                    <Card.Text>
                                        {content}
                                    </Card.Text>
                                </Card.Body>

                                <Card.Footer>
                                    Created at: {created_at ? created_at : ('returned null')}
                                </Card.Footer>
                            </Card>
                        )
                    })
                ) : (<h3>Loading News...</h3>)}
            </>
        )
    }
}

export default News
