import React, {Component} from 'react';
import Card from "react-bootstrap/Card";
import {withRouter} from "react-router";

class Category extends Component {
    state = {
        isLoading: true,
        articles: [],
        title: null,
        error: null
    };

    componentDidMount() {
        let id = this.props.match.params.id;
        this.fetchNews(id);
        this.fetchTitle(id);
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            let id = this.props.match.params.id;
            this.fetchNews(id);
            this.fetchTitle(id);
        }
    }

    fetchNews(id) {
        fetch(`http://apisrv.local/category/` + id)
            .then(response => response.json())
            .then(data => this.setState({
                articles: data,
                isLoading: false
            }))
            .catch(error => this.setState({error, isLoading: false}))
    }

    fetchTitle(id) {
        fetch(`http://apisrv.local/categories`)
            .then(response => response.json())
            .then(data => {
                data.map(temp => {
                    if (temp.id === id) {
                        this.setState({
                            title: temp.title,
                            isLoading: false
                        })
                    }
                })
            })
            .catch(error => this.setState({error, isLoading: false}))
    }

    render() {
        const {isLoading, articles, title, error} = this.state;

        return (
            <>
                <h2>{title} haberleri</h2>
                {error ? <p>{error.message}</p> : null}
                {!isLoading ? (
                    articles.map(article => {
                        const {id, title, content, created_at} = article;
                        return (
                            <Card key={id} className="mb-4">
                                <Card.Header as="h5">{title}</Card.Header>
                                <Card.Body>
                                    <Card.Text>
                                        {content}
                                    </Card.Text>
                                </Card.Body>

                                <Card.Footer>
                                    Created at: {created_at ? created_at : ('returned null')}
                                </Card.Footer>
                            </Card>
                        )
                    })
                ) : (<h3>Loading News...</h3>)}
            </>
        )
    }
}

export default withRouter(Category)
