import React, {Component} from 'react';
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import {Link} from "react-router-dom";

class Categories extends Component {
    state = {
        isLoading: true,
        categories: [],
        error: null
    };

    fetchNews() {
        fetch(`http://apisrv.local/categories`)
            .then(response => response.json())
            .then(data => this.setState({
                categories: data,
                isLoading: false
            }))
            .catch(error => this.setState({error, isLoading: false}))
    }

    componentDidMount() {
        this.fetchNews();
    }

    render() {
        const {isLoading, categories, error} = this.state;

        return (
            <>
                <Card>
                    <Card.Header>Categories</Card.Header>
                    <ListGroup variant="flush">
                        {error ? <p>{error.message}</p> : null}
                        {!isLoading ? (
                            categories.map(category => {
                                const {id, title} = category;
                                return (
                                    <ListGroup.Item key={id}><Link
                                        to={`/category/` + id}>{title}</Link></ListGroup.Item>
                                )
                            })
                        ) : (<Card.Body><h6>Loading Categories</h6></Card.Body>)}
                    </ListGroup>
                </Card>
            </>
        )
    }
}

export default Categories
