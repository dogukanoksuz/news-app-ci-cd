# Onepage News App

Written for CI/CD pipelining test. 

### Project contains two folders:

- React frontend app
- Lumen backend app

### How to install?

- yarn install on newsfrontend folder
- yarn start on newsfrontend folder
- install mysql server
- update .env file on newsapiserver folder
- composer update on newsapiserver folder
- composer-dumpautoload on newsapiserver folder
- php artisan migrate on api folder
- php artisan db:seed on api folder
- php -S localhost:3001 -t public on api folder

Ready to work!


