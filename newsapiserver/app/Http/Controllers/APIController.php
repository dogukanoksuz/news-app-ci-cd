<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;

class APIController extends Controller
{
    public function getAllNews()
    {
        return response()->json(Article::all());
    }

    public function getArticle($id)
    {
        return response()->json(Article::findOrFail($id)->get());
    }

    public function getCategories()
    {
        return response()->json(Category::all());
    }

    public function getCategoryPosts($id)
    {
        return response()->json(Article::where('category_id', $id)->get());
    }
}
