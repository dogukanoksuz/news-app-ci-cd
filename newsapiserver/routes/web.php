<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', [
    'as' => 'index', 'uses' => 'APIController@getAllNews'
]);

$router->get('/article/{id}', [
    'as' => 'article', 'uses' => 'APIController@getArticle'
]);

$router->get('/categories', [
    'as' => 'categories', 'uses' => 'APIController@getCategories'
]);

$router->get('/category/{id}', [
    'as' => 'news', 'uses' => 'APIController@getCategoryPosts'
]);
