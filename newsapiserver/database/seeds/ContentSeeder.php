<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = DB::table('categories');
        $categories->insert([
            ['title' => 'World'],
            ['title' => 'Economy'],
            ['title' => 'Sports']
        ]);

        $articles = DB::table('articles');
        $articles->insert([
            ['title'       => 'Is Divergent testing the API things?',
             'content'     => 'Lorem ipsum content things... I dont know what to write here... Just wanna create sum garbage content',
             'category_id' => 1],

            ['title'       => 'I dont think so',
             'content'     => 'Lorem ipsum content things... I dont know what to write here... Just wanna create sum garbage content',
             'category_id' => 2],

            ['title'       => 'Clearly something is missing',
             'content'     => 'Lorem ipsum content things... I dont know what to write here... Just wanna create sum garbage content',
             'category_id' => 3],
        ]);
    }
}
